#!/bin/bash
# ------------------------------------------------------------------
#  Acquia Backup Utility (version 0.2.1)
#    Manages user file and database backups in AWS S3
#
#  Dependencies:
#    HashBackup (build 1441 or later) - http://www.hashbackup.com/
#    AWS IAM account with S3 write access - http://blogs.aws.amazon.com/security/post/Tx3VRSWZ6B3SHAV/Writing-IAM-Policies-How-to-grant-access-to-an-Amazon-S3-bucket
#
#  Changelog:
#    20160125 - added missing sunny-day upgrade scenario
#    20160122 - rearchitected the file structure to allow future hb upgrades
#    20160121 - fixed time retention logic
#    20160121 - used proper Acquia environment variable to avoid symlink pathname changed warnings
#    20160121 - added verbosity option and changed arc filesize from 1 GB to 250 MB
#    20160119 - initial commit
#
# ------------------------------------------------------------------

# variables
TITLE="Acquia Backup Utility (version 0.2.1)"
USAGE="Usage: /var/www/html/\${AH_SITE_NAME}/backup/abu.sh backup [--verbose]|search file-folder-name|restore /full/path/to/file-folder-name [--rev #] [--origin]"
DESTCONF="/var/www/html/${AH_SITE_NAME}/backup/dest.conf"
OLDHB="/var/www/html/${AH_SITE_NAME}/backup/hb"
BACKUPDIR="/mnt/gfs/${AH_SITE_NAME}/hashbackup"
HB="${BACKUPDIR}/hb"

# display usage if no commands provided
if [ $# == 0 ] ; then

  cat << EOF

$TITLE

$USAGE

EOF
  exit 1;
fi

# process given command
case "$1" in
  "backup")
    echo " ** Verifying setup..."
#   check for upgrade
    test -f $HB && $HB upgrade --force
#   if hashbackup folder doesn't exist, first assume it exists in s3 so try to recover
#   if that doesn't work then assume a new installation and initialize
    test -d $BACKUPDIR \
    || (mkdir $BACKUPDIR \
    && cp $DESTCONF $BACKUPDIR \
    && cp $OLDHB $HB \
    && chmod +x $HB \
    && echo '# HashBackup Key File - DO NOT EDIT!' > $BACKUPDIR/key.conf \
    && echo 'Keyfrom user' >> $BACKUPDIR/key.conf \
    && echo 'Key' >> $BACKUPDIR/key.conf \
    && $HB upgrade --force \
    && $HB recover -c $BACKUPDIR -n --force) \
    || (rm -rf $BACKUPDIR \
    && $OLDHB init -c $BACKUPDIR -k '' \
    && cp $DESTCONF $BACKUPDIR \
    && cp $OLDHB $HB \
    && chmod +x $HB \
    && $HB upgrade --force \
    && $HB config -c $BACKUPDIR cache-size-limit 0 \
    && $HB config -c $BACKUPDIR arc-size-limit 250mb \
    && $HB config -c $BACKUPDIR copy-executable True \
    && $HB backup -c $BACKUPDIR /dev/null \
    && $HB retain -c $BACKUPDIR -t 21d \
    && $HB selftest -c $BACKUPDIR)
    echo "Verification complete"
#   back up docroot
    echo " ** Backing up databases..."
    VFLAG='-v 0' && [[ $2 == '--verbose' ]] && VFLAG='-v 2'
    $HB backup -c $BACKUPDIR $VFLAG /mnt/gfs/${AH_SITE_NAME}/backups/*.sql.gz
    echo " ** Backing up files..."
    $HB backup -c $BACKUPDIR $VFLAG /mnt/gfs/${AH_SITE_NAME}/sites/*/files*
    ;;
  "search")
#   check all revisions to include deleted files; regex supported
    $HB ls -c $BACKUPDIR -a | grep "$2"
    ;;
  "restore")
    if [[ $2 != /* ]]; then echo "Error: restore path must start with '/'" && exit 1; fi
    ORIGFLAG='' && [[ $3 == '--origin' || $5 == '--origin' ]] && ORIGFLAG='--orig'
    REVFLAG='' && [[ $3 == '--rev' && $4 =~ ^-?[0-9]+$ ]] && REVFLAG="-r $4"
    [[ $4 == '--rev' && $5 =~ ^-?[0-9]+$ ]] && REVFLAG="-r $5"
    if [[ $ORIGFLAG == '' ]]; then [ -d /tmp/restored ] || mkdir /tmp/restored ; cd /tmp/restored ; fi
    $HB get -c $BACKUPDIR $2 $REVFLAG $ORIGFLAG
    ;;
  *)
    cat << EOF

$TITLE

$USAGE

EOF
    exit 1;
    ;;
esac
