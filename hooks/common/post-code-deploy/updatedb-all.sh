#!/bin/sh
#
# Cloud Hook: updatedb-all
#
# Run updatedb on all sites, if needed, otherwise clear cache.

site="$1"
target_env="$2"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  updatedb_status=`drush @$site.$target_env updatedb-status`
  if [ ! -z "$updatedb_status" ]; then
    drush @sites updatedb --yes
  else
    drush @$site.$target_env cache-clear all
  fi
fi
