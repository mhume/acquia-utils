#!/bin/sh
#
# Cloud Hook: registry-rebuild
#
# Rebuilds the Drupal registry.

site="$1"
target_env="$2"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  drush @$site.$target_env dl registry_rebuild --yes
  drush @$site.$target_env registry-rebuild --no-cache-clear
fi
