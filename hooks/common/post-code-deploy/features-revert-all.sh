#!/bin/sh
#
# Cloud Hook: features-revert-all
#
# Revert all Drupal Features.

site="$1"
target_env="$2"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  drush @$site.$target_env features-revert-all --yes
fi
