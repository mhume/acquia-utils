#!/bin/sh
#
# Cloud Hook: reindex
#
# Reindex search when a new database is deployed.

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  drush @$site.$target_env solr-delete-index
  drush @$site.$target_env solr-mark-all
  drush @$site.$target_env solr-index
fi
