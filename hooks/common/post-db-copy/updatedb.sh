#!/bin/sh
#
# Cloud Hook: updatedb
#
# Run updatedb, if needed, otherwise clear cache.

site="$1"
target_env="$2"

if [ "$AH_SITE_ENVIRONMENT" != "ra" ]; then
  updatedb_status=`drush @$site.$target_env updatedb-status`
  if [ ! -z "$updatedb_status" ]; then
    drush @$site.$target_env updatedb --yes
  else
    drush @$site.$target_env cache-clear all
  fi
fi
